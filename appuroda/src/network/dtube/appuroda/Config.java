package network.dtube.appuroda;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Similar logic to permanete.config
 * 
 * @author vaultec
 */
public class Config {
	transient File temp_file; //Calculated on launch
	transient File config_savelocation; //Calculated on launch.
	
	
	
	public Video video = new Video();
	public General general = new General();
	
	public class General {
		//When enabled, the encoded videos are sent to a remote IPFS server.
		boolean useRemoteServer = false;
		String remoteServerAddress = ""; //HTTP host.
		int http_port = 8078;
		
		public String IPFS_HOST = "/ip4/127.0.0.1/tcp/5001";
	}
	
	public class Video {
		//Maximum time for encoding.
		int EncodeTimeout = 108000;
		//Number of Sprite images to create.
		int NbSpriteImages = 100;
		//Maximum video duration to encode
		int MaxVideoDurationForEncoding = 1800;
		//How many encoding daemons should be running.
		int EncodeDaemons = 4;
		//The quality that is authorized for encoding. default 240p, 480p, 720p
		ArrayList<String> AuthorizedQuality = new ArrayList<String>(Arrays.asList("240p","480p", "720p"));
	}
}
