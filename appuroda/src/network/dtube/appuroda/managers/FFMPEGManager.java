package network.dtube.appuroda.managers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.rauschig.jarchivelib.ArchiveFormat;
import org.rauschig.jarchivelib.Archiver;
import org.rauschig.jarchivelib.ArchiverFactory;

import fr.rhaz.ipfs.IPFSDaemon;

public class FFMPEGManager {
	public File ffmpeg_path;
	public File ffprobe_path;

	private String ffmpeg_windows = "https://ffmpeg.zeranoe.com/builds/win64/static/ffmpeg-latest-win64-static.zip";
	private String ffmpeg_macosx = "https://ffmpeg.zeranoe.com/builds/macos64/static/ffmpeg-latest-macos64-static.zip";
	private String ffmpeg_linux = "https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz";

	private String temp;
	private String os_name;
	private File ffmpeg_dir;

	
	public FFMPEGManager() {
		temp = System.getProperty("java.io.tmpdir");
		os_name = System.getProperty("os.name");
		
		if(os_name.contains("Mac")) {
			ffmpeg_dir = new File(temp+"/ffmpeg/"+"ffmpeg-latest-macos64-static");
		} else if(os_name.contains("Linux")) {
			ffmpeg_dir = new File(temp+"/ffmpeg/"+"ffmpeg-release-amd64-static");
		} else if(os_name.contains("Windows")) {
			ffmpeg_dir = new File(temp+"/ffmpeg-latest-win64-static");
			this.ffmpeg_path = new File(ffmpeg_dir.toString() + "/bin/ffmpeg.exe");
			this.ffprobe_path = new File(ffmpeg_dir.toString() + "/bin/ffprobe.exe");
		}
		if(ffmpeg_path == null && ffprobe_path == null) {
			this.ffmpeg_path = new File(ffmpeg_dir.toString() + "/bin/ffmpeg");
			this.ffprobe_path = new File(ffmpeg_dir.toString() + "/bin/ffprobe");
		}
	}
	
	private void FileDownload(String in, File location) throws Exception {
		URL website = new URL(in);
		HttpsURLConnection con = (HttpsURLConnection) website.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("User-Agent", "Wget/1.13.4");
		con.connect();
		InputStream inputstream = con.getInputStream();

		FileOutputStream out = new FileOutputStream(location);
		byte[] buf = new byte[1024];
		int len;
		while ((len = inputstream.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		out.close();
		inputstream.close();
	}
	
	public boolean isInstalled() {
		return ffmpeg_dir.exists();
	}
	
	/**
	 * updates FFMPEG To latest version.
	 * Returns success or failure
	 * @return
	 */
	public boolean updateFFMPEG() {
		ffmpeg_dir.delete();
		return this.installToTemp();
	}
	
	/**
	 * Installs ffmpeg into temp file; Returns boolean of success.
	 * 
	 * @return
	 */
	public boolean installToTemp() {
		if (os_name.contains("Mac")) {
			File location = new File(temp + "ffmpeg.zip");
			if(!ffmpeg_dir.exists()) {
				try {
					FileDownload(this.ffmpeg_macosx, location);
				} catch (Exception e1) {
					e1.printStackTrace();
					return false;
				}
				Archiver archiver = ArchiverFactory.createArchiver(ArchiveFormat.ZIP);
				try {
					archiver.extract(location, ffmpeg_dir);
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
		} else if (os_name.contains("Linux")) {
			File location = new File(temp + "ffmpeg.tar.gz");
			if (!ffmpeg_dir.exists()) {
				try {
					FileDownload(this.ffmpeg_linux, location);
				} catch (Exception e1) {
					e1.printStackTrace();
					return false;
				}
				Archiver archiver = ArchiverFactory.createArchiver("tar", "gz");
				try {
					archiver.extract(location, ffmpeg_dir);
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
		} else if (os_name.contains("Windows")) {
			File location = new File(temp + "ffmpeg.zip");
			//If its not already installed
			if (!ffmpeg_dir.exists()) {
				try {
					FileDownload(this.ffmpeg_windows, location);
				} catch (Exception e1) {
					e1.printStackTrace();
					return false;
				}
				Archiver archiver = ArchiverFactory.createArchiver(ArchiveFormat.ZIP);
				try {
					archiver.extract(location, new File(this.temp));
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
			this.ffmpeg_path = new File(ffmpeg_dir.toString() + "/bin/ffmpeg.exe");
			this.ffprobe_path = new File(ffmpeg_dir.toString() + "/bin/ffprobe.exe");
		}
		if(ffmpeg_path == null && ffprobe_path == null) {
			this.ffmpeg_path = new File(ffmpeg_dir.toString() + "/bin/ffmpeg");
			this.ffprobe_path = new File(ffmpeg_dir.toString() + "/bin/ffprobe");
		}
		
		this.ffmpeg_path.setExecutable(true);
		this.ffprobe_path.setExecutable(true);
		return true;
	}
}
