package network.dtube.appuroda;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;

import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFmpegUtils;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import net.bramp.ffmpeg.job.FFmpegJob;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import net.bramp.ffmpeg.probe.FFmpegStream;
import net.bramp.ffmpeg.progress.Progress;
import net.bramp.ffmpeg.progress.ProgressListener;
import network.dtube.appuroda.managers.FFMPEGManager;
import network.dtube.appuroda.models.RawUpload;
import network.dtube.appuroda.models.Resolutions;
/**
 * 
 * @author vaultec
 */
public class EncodingDaemon extends Thread {
	private RawUpload upload;
	private UploadTasker uploadTasker;
	
	private final FFmpeg ffmpeg;
	private final FFprobe ffprobe;
	private final FFmpegExecutor executor;
	
	public EncodingDaemon(FFMPEGManager ffmpegManager) throws IOException {
		ffmpeg = new FFmpeg(ffmpegManager.ffmpeg_path.toString());	
		ffprobe = new FFprobe(ffmpegManager.ffprobe_path.toString());
		executor = new FFmpegExecutor(ffmpeg, ffprobe);
	}
	public EncodingDaemon(FFmpeg ffmpeg, FFprobe ffprobe, FFmpegExecutor executor) throws IOException {
		this.ffmpeg = ffmpeg;
		this.ffprobe = ffprobe;
		this.executor = executor;
	}
	/**
	 * Sets the video to process
	 * @param upload
	 */
	public void setRawUpload(RawUpload upload) {
		this.upload = upload;
	}
	public void setUploadTasker(UploadTasker in) {
		this.uploadTasker = in;
	}
	
	public boolean isBusy() {
		if(upload == null) {
			return false;
		}
		return !upload.finished;
	}
	public void run() {
		
		for( ; ; ) {
			if(upload != null && !upload.finished) {
				HashMap<String, File> EncodedResults = new HashMap<String, File>();
				FFmpegProbeResult sourceVideo = null;
				
				try {
					sourceVideo = ffprobe.probe(upload.srcVideoLocation.toString());
				} catch (IOException e1) {
					e1.printStackTrace();
					break;
				}
				System.out.println(new Gson().toJson(sourceVideo));
				System.out.println("Running encoding cycle.");
				FFmpegStream streamToEncode = null;
				for(FFmpegStream e : sourceVideo.streams) {
					System.out.println(new Gson().toJson(e));
					if(e.height > 1) {
						streamToEncode = e;
						break;
					}
				}
				
				System.out.println(new Gson().toJson(streamToEncode));
				//Encoding cycle;
				for(Resolutions.res res : upload.enabledEncodings) {
					//Only encode if the resolution is higher than requested resolution
					System.out.println(sourceVideo.streams.get(0).height);
					//if(streamToEncode.height >= res.height() && streamToEncode.width >= res.width()) {
						System.out.println(streamToEncode.height);
						//Preparing
						
						File output;
						try {
							output = Files.createTempFile("videoEncode"+res.urlTag(), ".mp4").toFile();
						} catch (IOException e) {
							e.printStackTrace();
							break;
						}
						output.deleteOnExit();
						System.out.println("bitrate " + res.maxRate());
						
						//FFMPEG Builder
						FFmpegBuilder builder = new FFmpegBuilder()
						.setInput(this.upload.srcVideoLocation.toString())
						.addOutput(output.toString())
						.setFormat("mp4")
						//Audio
						.setAudioChannels(2)
						.setAudioCodec("aac")
						//Video
						.setVideoCodec("libx264")
						.setVideoFrameRate(30, 1) //30 FPS
						.setVideoResolution(res.width(), res.height())
						.setVideoBitRate(res.maxRate())
						.done();
						
						System.out.println("Resolution: "+res.width()+"x"+ res.height());
						
						final FFmpegProbeResult sourceVideoFinal = sourceVideo;
						FFmpegJob job = executor.createJob(builder, new ProgressListener() {
							final double duration_ns  = sourceVideoFinal.getFormat().duration * TimeUnit.SECONDS.toNanos(1);
							
							@Override
							public void progress(Progress progress) {
								double percentage = progress.out_time_ns / duration_ns;
								//double percentage = progress.out_time_ns / duration_ns;
								// Print out interesting information about the progress
								/*System.out.println(String.format(
									"[%.0f%%] status:%s frame:%d time:%s ms fps:%.0f speed:%.2fx",
									//percentage * 100,
									//progress.status,
									progress.frame,
									FFmpegUtils.toTimecode(progress.out_time_ns, TimeUnit.NANOSECONDS),
									progress.fps.doubleValue(),
									progress.speed
								));*/
								System.out.println(String.format(
										"[%.0f%%]",
										percentage * 100
									));
								System.out.println(progress.toString());
								System.out.println(((float) progress.out_time_ns / (float) duration_ns ));
								System.out.println(progress.out_time_ns);
								System.out.println(this.duration_ns );
								
							}
						});
						job.run();
						System.out.println(output);
						EncodedResults.put(res.urlTag(), output);
						IPFS ipfs = new IPFS("/ip4/127.0.0.1/tcp/5001");
						NamedStreamable.FileWrapper file = new NamedStreamable.FileWrapper(output);
						try {
							MerkleNode addResult = ipfs.add(file).get(0);
							System.out.println(res.urlTag() +": "+ addResult.hash.toBase58());
						} catch (IOException e) {
							e.printStackTrace();
						}
					//}
				}
				//Have IPFS file adding here.
				/**
				 * Upload to remote IPFS start if needed
				 */
				
				
				//End set the video status to completed;
				
				System.out.println("Encoding cycle completed");
			}
			
			
			
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
