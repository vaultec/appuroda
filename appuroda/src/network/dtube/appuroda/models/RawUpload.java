package network.dtube.appuroda.models;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import network.dtube.appuroda.models.Resolutions.res;

/**
 * Entry acts as a VideoEntry(from permanente) but is for uploads.
 * 
 * @author vaultec
 */
public class RawUpload {
	public long startTime;
	
	public boolean finished;
	
	
	public List<VideoResult> encodedVideos = new ArrayList<VideoResult>();
	
	public transient List<Resolutions.res> enabledEncodings = new ArrayList<Resolutions.res>();
	public transient File srcVideoLocation;
	
}