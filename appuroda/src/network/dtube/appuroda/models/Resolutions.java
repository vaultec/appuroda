package network.dtube.appuroda.models;

public interface Resolutions {
	enum enums {
		r144p, r240p, r480p, r720p, r1080p;
	}
	
	interface res {
		//public transient int progress; //0 to 100% progress monitor
		
		/*public String urlTag;
		public long maxRate;
		public int width;
		public int height;
		public int MinSourceHeightForEncoding;
		public int qualityOrder;*/
		
		public String urlTag();
		public long maxRate();
		public int width();
		public int height();
		public int MinSourceHeightForEncoding();
		
	}
	public class r144p implements res {
		String urlTag = "144p";
		long maxRate = 100 * 1000;
		int width = 256;
		int height = 144;
		int MinSourceHeightForEncoding = 80;
		int qualityOrder = 1;
		
		@Override
		public String urlTag() {
			return urlTag;
		}
		@Override
		public long maxRate() {
			return maxRate;
		}
		@Override
		public int width() {
			return width;
		}
		@Override
		public int height() {
			return height;
		}
		@Override
		public int MinSourceHeightForEncoding() {
			return MinSourceHeightForEncoding;
		}
	}
	public class r240p implements res {
		String urlTag = "240p";
		long maxRate = 200 * 1000;
		int width = 426;
		int height = 240;
		int MinSourceHeightForEncoding = 120;
		int qualityOrder = 2;
		@Override
		public String urlTag() {
			return urlTag;
		}
		@Override
		public long maxRate() {
			return maxRate;
		}
		@Override
		public int width() {
			return width;
		}
		@Override
		public int height() {
			return height;
		}
		@Override
		public int MinSourceHeightForEncoding() {
			return MinSourceHeightForEncoding;
		}
	}
	public class r480p implements res {
		String urlTag = "480p";
		long maxRate = 500 * 1000;
		int width = 854;
		int height = 480;
		int MinSourceHeightForEncoding = 360;
		int qualityOrder = 3;
		@Override
		public String urlTag() {
			return urlTag;
		}
		@Override
		public long maxRate() {
			return maxRate;
		}
		@Override
		public int width() {
			return width;
		}
		@Override
		public int height() {
			return height;
		}
		@Override
		public int MinSourceHeightForEncoding() {
			return MinSourceHeightForEncoding;
		}
	}
	public class r720p implements res {
		String urlTag = "720p";
		long maxRate = 200 * 1000;
		int width = 1080;
		int height = 720;
		int MinSourceHeightForEncoding = 600;
		int qualityOrder = 4;
		@Override
		public String urlTag() {
			return urlTag;
		}
		@Override
		public long maxRate() {
			return maxRate;
		}
		@Override
		public int width() {
			return width;
		}
		@Override
		public int height() {
			return height;
		}
		@Override
		public int MinSourceHeightForEncoding() {
			return MinSourceHeightForEncoding;
		}
	}
	public class r1080p implements res{
		String urlTag = "1080p";
		long maxRate = 3200 * 1000;
		int width = 1920;
		int height = 1080;
		int MinSourceHeightForEncoding = 900;
		int qualityOrder = 5;
		
		@Override
		public String urlTag() {
			return urlTag;
		}
		@Override
		public long maxRate() {
			return maxRate;
		}
		@Override
		public int width() {
			return width;
		}
		@Override
		public int height() {
			return height;
		}
		@Override
		public int MinSourceHeightForEncoding() {
			return MinSourceHeightForEncoding;
		}
	}
}
