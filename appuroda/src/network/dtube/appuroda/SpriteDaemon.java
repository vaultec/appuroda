package network.dtube.appuroda;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;

import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import net.bramp.ffmpeg.probe.FFmpegStream;
import network.dtube.appuroda.managers.FFMPEGManager;
import network.dtube.appuroda.models.RawUpload;

public class SpriteDaemon extends Thread {
	
	private RawUpload upload;
	private UploadTasker uploadTasker;
	
	private final FFmpeg ffmpeg;
	private final FFprobe ffprobe;
	private final FFmpegExecutor executor;
	private int max_sprites = 100; //Default DTube settings don't change
	
	
	public SpriteDaemon(FFMPEGManager ffmpegManager) throws IOException {
		ffmpeg = new FFmpeg(ffmpegManager.ffmpeg_path.toString());	
		ffprobe = new FFprobe(ffmpegManager.ffprobe_path.toString());
		executor = new FFmpegExecutor(ffmpeg, ffprobe);
	}
	public void setRawUpload(RawUpload upload) {
		this.upload = upload;
	}
	public void setUploadTasker(UploadTasker in) {
		this.uploadTasker = in;
	}
	public boolean isBusy() {
		if(upload == null) {
			return false;
		}
		return !upload.finished;
	}
	private FFmpegStream getVideoStream(final FFmpegProbeResult in) {
		for(FFmpegStream e : in.streams) {
			if(e.codec_type.toString() == "VIDEO") {
				return e;
			}
		}
		return null;
	}
	public File CreateSprite(File inVideo) throws IOException {
		final FFmpegStream sourceVideo = getVideoStream(ffprobe.probe(upload.srcVideoLocation.toString())); //Probe results from video
		File temp_dir = Files.createTempDirectory("imageMagick-appuroda").toFile();  temp_dir.mkdir(); //Temp directory for all the sprites!
		
		int avg_fps = sourceVideo.avg_frame_rate.intValue();
		double duration = sourceVideo.duration;
		
		int duration_sec = (int) Math.round(duration);
		int sprites = 0;
		
		if(max_sprites > duration_sec) {
			sprites = duration_sec;
		} else {
			sprites = this.max_sprites;
		}
		int step_sec = duration_sec/sprites;
		
		
		int step_frames = Math.round(step_sec * sourceVideo.avg_frame_rate.floatValue());
		ArrayList<Integer> intervals = new ArrayList<Integer>(Arrays.asList(0));
		int last = 0;
		for(int x = 0; x < sprites; x++) {
			last=+step_frames;
			intervals.add(last);
		}
		ArrayList<File> sprites_images = new ArrayList<File>();
		for( Integer e : intervals) {
			
		}
		
		
		return new File("");
	}
	
	
	
	
	@Override
	public void run() {
		
	}
}
