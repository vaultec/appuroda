package network.dtube.appuroda;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;
import java.util.UUID;

import org.apache.commons.cli.Options;

import io.ipfs.api.IPFS;
import network.dtube.appuroda.managers.FFMPEGManager;
import network.dtube.appuroda.models.RawUpload;
import network.dtube.appuroda.models.Resolutions;

/**
 * Main class for uploader.. Long continueous processes should run as side threads.
 * 
 * @author vaultec
 * LICENSE: GPLv2
 */
public class Appuroda implements Runnable {
	//String is the unique upload token. RawUpload for the entries being upload. Temporary, in memory only.
	TreeMap<String, RawUpload> UploadIndex = new TreeMap<String, RawUpload>();
	
	public FFMPEGManager ffmpegManager = new FFMPEGManager();
	public UploadTasker uploadTasker = new UploadTasker(this);
	public IPFS ipfs;
	public Config config;
	
	public boolean closing;
	
	
	public Appuroda() {
		System.out.println(ffmpegManager.isInstalled());
		if(!ffmpegManager.isInstalled()) {
			ffmpegManager.installToTemp();
		}
		System.out.println(this.ffmpegManager.ffmpeg_path.toString());
		this.config = new Config(); //default config for now
	}
	/**
	 * Adds Video to upload queue/uploadindex. Returns unique upload token.
	 * @param location
	 * @param ResolutionsToEncode
	 * @return
	 */
	public String uploadVideoFromFile(File location, List<Resolutions.res> ResolutionsToEncode){
		String token = UUID.randomUUID().toString();
		RawUpload upload = new RawUpload();
		upload.srcVideoLocation = location;
		upload.enabledEncodings = ResolutionsToEncode;
		this.UploadIndex.put(token, upload);
		
		return token;
	}
	public void uploadSnapFromFile(File location, String token) {
		if(!this.UploadIndex.containsKey(token)) {
			return;
		}
		
	}
	
	@Override
	public void run() {
		this.uploadTasker.start(); //Light off upload tasking.
	}
	public static void main(String[] args) throws IOException {
		
		IpfsDaemon ipfsd = new IpfsDaemon();
		try {
			ipfsd.download();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(true)
			System.exit(0);
		Appuroda uploader = new Appuroda();
		Options options = new Options();
		System.out.println(System.getProperty("java.io.tmpdir"));
		//System.out.println(manager.ffmpeg_path.toString());
		File fileToEncode = new File(System.getProperty("user.home")+"/Desktop/test_video.mp4");
		String token = uploader.uploadVideoFromFile(fileToEncode, Arrays.asList(new Resolutions.r480p(), new Resolutions.r144p()));
		
		uploader.run();
		
		
		
		//Process process = new ProcessBuilder(manager.ffmpeg_path.toString()).start();
		
		
		
		
		/*InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;

		System.out.printf("Output of running %s is:", Arrays.toString(args));

		while ((line = br.readLine()) != null) {
		  System.out.println(line);
		}*/
	}
}
