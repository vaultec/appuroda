package network.dtube.appuroda;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.SystemUtils;
import org.rauschig.jarchivelib.ArchiveEntry;
import org.rauschig.jarchivelib.ArchiveFormat;
import org.rauschig.jarchivelib.ArchiveStream;
import org.rauschig.jarchivelib.Archiver;
import org.rauschig.jarchivelib.ArchiverFactory;

import com.google.common.collect.Lists;
import com.google.common.hash.HashFunction;
import com.google.common.io.Files;

import io.ipfs.api.IPFS;

public class IpfsDaemon {
	private Process daemon = null;
	
	public ArrayList<String> args = new ArrayList<String>();
	
	
	private String version;
	private File path;
	private File bin;
	
	public IpfsDaemon() {
		this("0.4.20", new File(System.getProperty("java.io.tmpdir")));
	}
	public IpfsDaemon(String version, File path) {
		this.version = version;
		this.path = path;
		if(SystemUtils.IS_OS_WINDOWS) this.bin = new File(path + "/ipfs.exe");
		else this.bin = new File(path + "/ipfs");
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
	        public void run() {
	        	if(daemon != null && daemon.isAlive()) {
	        		daemon.destroyForcibly();
	        	}
	        }
	    });
	}
	private Exception err = new Exception("System not supported");
	private String arch() throws Exception {
		switch(SystemUtils.OS_ARCH) {
		case "amd64":return "amd64";
		case "x86":return "386";
		case "x86_64":return "386";
		case "ia64":return "386";
		case "arm":return "arm";
		default:throw err;
		}
	}
	private List<String> type() throws Exception {
		//Please test all these!
		if(SystemUtils.IS_OS_WINDOWS) return Arrays.asList("windows", ".zip");
		else if(SystemUtils.IS_OS_LINUX) return Arrays.asList("linux", ".tar.gz");
		else if(SystemUtils.IS_OS_FREE_BSD) return Arrays.asList("freebsd", ".tar.gz");
		else if(SystemUtils.IS_OS_MAC) return Arrays.asList("darwin", ".tar.gz"); 
		else throw err;
	}
	public IPFS api() {
		return new IPFS("/ip4/127.0.0.1/tcp/5001");
	}
	public void download() throws Exception {
		download(version, bin);
	}
	public void download(String version, File bin) throws Exception {
		System.out.println(path);
		if(bin.exists()) return;
		String type = type().get(1); String os = type().get(0);
		
		
		File tmparchive = new File(SystemUtils.getJavaIoTmpDir()+"/go-ipfs"+type);
		String url = "https://dist.ipfs.io/go-ipfs/v"+version+"/go-ipfs_v"+version+"_"+os+"-"+arch()+type;
		System.out.println("download path: " + url);
		System.out.println("archive location:" + tmparchive);
		
		downloadFileFromURL(new URL(url), tmparchive);
		String archivepath = "go-ipfs/ipfs";
		if(SystemUtils.IS_OS_WINDOWS) archivepath += ".exe";
		
		switch(type) {
		case ".tar.gz": extractTarGz(tmparchive, archivepath, bin);
		case ".zip": extractZip(tmparchive, archivepath, bin);
		}
		
		bin.setExecutable(true);
		System.out.println("archivepath " + archivepath);
		System.out.println("tmparchive " + tmparchive);
		
		System.out.println(tmparchive);
		System.out.println("bin: " +  bin);
		
	} 
	public void start() {
		Process old = daemon;
		if(old != null && old.isAlive()) return;
		
		try {
			process(Collections.singletonList("init")).waitFor(); //Init
			
			List<String> args = Arrays.asList("daemon");args.addAll(args); //Start dae,pm
			this.daemon = process(args);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	private Process process(List<String> args) throws IOException {
		return process(bin, new File(System.getProperty("user.home") + ".ipfs"), args);
	}
	private Process process(File bin, File store, List<String> args) throws IOException {
		String cmd = bin.getPath() + String.join(" ", args);
		return Runtime.getRuntime().exec(cmd, (String[]) Arrays.asList("IPFS_PATH="+store.getAbsolutePath()).toArray());
	}
	private void downloadFileFromURL(URL url, File file) throws IOException {
		  ReadableByteChannel rbc = Channels.newChannel(url.openStream());
		  FileOutputStream fos = new FileOutputStream(file);
		  fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		  fos.close();
	}
	private static void extractTarGz(File arch, String path, File destination) throws IOException {
		ArchiveStream archive =  ArchiverFactory.createArchiver("tar", "gz").stream(arch);
		OutputStream out = new FileOutputStream(destination);
		ArchiveEntry ar = null;
		while((ar = archive.getNextEntry()) != null) {
			if(ar.getName().equals(path)) {
				byte[] buffer = new byte[9000];
	            int len;
	            while ((len = archive.read(buffer)) != -1) {
	            	out.write(buffer, 0, len);
	            }
	            out.close();
	            break;
			}
		}
	}
	private static void extractZip(File zip, String path, File destination) throws IOException {
		ArchiveStream archive =  ArchiverFactory.createArchiver(ArchiveFormat.ZIP).stream(zip);
		OutputStream out = new FileOutputStream(destination);
		ArchiveEntry ar = null;
		while((ar = archive.getNextEntry()) != null) {
			if(ar.getName().equals(path)) {
				byte[] buffer = new byte[9000];
	            int len;
	            while ((len = archive.read(buffer)) != -1) {
	            	out.write(buffer, 0, len);
	            }
	            out.close();
	            break;
				
			}
		}
	}
}
