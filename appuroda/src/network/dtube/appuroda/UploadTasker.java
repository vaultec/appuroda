package network.dtube.appuroda;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;

import network.dtube.appuroda.models.RawUpload;

public class UploadTasker extends Thread {
	private Appuroda appuroda;
	ArrayList<EncodingDaemon> EncodingDaemons = new ArrayList<EncodingDaemon>();

	public UploadTasker(Appuroda appuroda) {
		this.appuroda = appuroda;
	}

	private void fireUpEncodingDaemon() {
		// Fire up our encoding threads.
		for (int x = 0; x < appuroda.config.video.EncodeDaemons; x++) {
			System.out.println("Started encoding thread: " + x);
			EncodingDaemon daemon = null;
			try {
				daemon = new EncodingDaemon(this.appuroda.ffmpegManager);
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
			daemon.setUploadTasker(this);
			daemon.start();
			this.EncodingDaemons.add(daemon);
		}
	}
	public void addToDaemon(String token, RawUpload upload) {
		for (EncodingDaemon e : this.EncodingDaemons) {
			// System.out.println(!e.isBusy());
			if (!e.isBusy()) {
				e.setRawUpload(upload);
				break;
				// System.out.println(new Gson().toJson(upload));
			}
		}
	}
	@Override
	public void run() {
		fireUpEncodingDaemon();
		for ( ; ; ) {
			Iterator<String> it = this.appuroda.UploadIndex.keySet().iterator();
			if (it.hasNext()) {
				String token = it.next();
				RawUpload upload = this.appuroda.UploadIndex.get(token);
				// System.out.println(new Gson().toJson(upload));
				// System.out.println(token);
				// System.out.println(this.EncodingDaemons.size());
				this.addToDaemon(token, upload);
			}
			

			if (appuroda.closing) {

				break;
			}
			try {
				TimeUnit.MILLISECONDS.sleep(1); // So we dont kill the CPU
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
